
<div class="row">
  <div class="col-md-12">
      <div class="panel panel-primary">   
      <!-- Table -->
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Edit</th>
              <th>Del</th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; foreach($categories as $list):?>
            <tr>
              <td><?php echo $i++;?></td>
              <td><?php echo $list->name;?></td>
              <td><?php echo $list->description;?></td>
              <td><a href="<?php echo base_url('categories/edit_categories').'/'.$list->id;?>" class="btn btn-success btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
              <td><a href="<?php echo base_url('categories/del_categories').'/'.$list->id;?>" class="btn btn-warning btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a></td>
            </tr>
            <?php endforeach;?>
          </tbody>
        </table>
      </div>
    </div>  
  </div>
</div>
