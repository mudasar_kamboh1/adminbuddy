<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('categories_model');
        $this->load->helper('my_helper');
        if(!$this->session->userdata('logged_in')){
            redirect('admin');
	    }
    }
    
	public function index(){
        $data['categories'] = get_all('categoroies');        
	    $data['title'] = 'Categories';
        $data['view']  = 'categories';
        $this->load->view('layout',$data);
	}

    // Edit Categories
    public function edit_categories(){
        $data['title'] = 'Edit Category';
        $data['view']  = 'edit_cat';
        $this->load->view('layout',$data);
    }
}

