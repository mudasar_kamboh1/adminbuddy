<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      CodeR 
 * @copyright   (c) 2016, CMS Development
 * @since       Version 0.1
 */
class Items_model extends My_Model {

    public function __construct() {
        parent::__construct();
    }

    // Get All Categories
    public function get_all_items() {
        $this->db->select('i.* , c.name as category_name');
        $this->db->from('items as i');
        $this->db->join('categoroies as c','c.id = i.category_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function add_items_data($data) {
        $this->db->insert('items', $data);
        return $this->db->insert_id();
    }
    public function add_items_detail($data) {
        $this->db->insert('items_detail', $data);
    }
    public function remove_item($id) {
        $this->db->where('id',$id);
        $this->db->delete('items');
    }

    public function get_item($id)
    {
        $this->db->select('i.* , c.name as category_name');
        $this->db->from('items as i');
        $this->db->join('categoroies as c','c.id = i.category_id');
        $this->db->where('i.id',$id);
        $query = $this->db->get();
        return $query->row();
    }
    public function get_item_detail($id)
    {
        $this->db->select('id.*');
        $this->db->from('items as i');
        $this->db->join('items_detail as id','id.item_id = i.id');
        $this->db->where('i.id',$id);
        $query = $this->db->get();
        return $query->result();
    }

    public function item_update($id,$data) {
        $this->db->where('id',$id);
        $this->db->update('items_detail', $data);
    }

}