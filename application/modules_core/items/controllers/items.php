<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Items extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('items_model');
        $this->load->helper('my_helper');
        if(!$this->session->userdata('logged_in')){
            redirect('admin');
	    }
    }
    
	public function index(){
        $data['items'] = $this->items_model->get_all_items();
	    $data['title'] = 'Items';
        $data['view']  = 'items';
        $this->load->view('layout',$data);
	}
    
    // View off add items
    public function add_item()
    {
        $data['title'] = 'Add Item';
        $data['view']  = 'add_item';
        $data['categories'] = get_all('categoroies');
        $this->load->view('layout',$data);
    }

    // Add items in db
    public function add_items_data()
    {
        $data = array(
            'category_id' => $this->input->post('category_id'), 
            'suplier_id' => $this->input->post('suplier_id'), 
            'item_name' => $this->input->post('item_name'), 
            'item_description' => $this->input->post('item_description'), 
            'item_quantity' => $this->input->post('item_quantity'), 
            'previous_price' => $this->input->post('previous_price'), 
            'serving_price' => $this->input->post('serving_price') 
        );
        $response = $this->items_model->add_items_data($data);        
        $barcodes = $this->input->post('barcodes');
        foreach($barcodes as $k => $value){
            $this->items_model->add_items_detail(array('item_id'=>$response,'barcode'=>$value));
        }
        redirect('items');
    }

    // Edit Items 
     public function edit_item(){        
        if($this->input->get('id')){
            $id = $this->input->get('id');
            $data['title'] = 'Edit Item';
            $data['view']  = 'edit_item';
            $data['categories'] = get_all('categoroies');
            $data['item'] = $this->items_model->get_item($id);
            //print_r($data['item']);exit;
            $data['item_detail'] = $this->items_model->get_item_detail($id);
            $this->load->view('layout',$data);
        }else{
            redirect('items');
        }
    }

    // Edit Item Update
    public function item_update()
    {
        $item_id = $this->input->post('id');
        $data = array(
            'category_id' => $this->input->post('category_id'), 
            'suplier_id' => $this->input->post('suplier_id'), 
            'item_name' => $this->input->post('item_name'), 
            'item_description' => $this->input->post('item_description'), 
            'item_quantity' => $this->input->post('item_quantity'), 
            'previous_price' => $this->input->post('previous_price'), 
            'serving_price' => $this->input->post('serving_price'),
            'updated_at' => date('Y-m-d')
        );
        $this->items_model->item_update($item_id,$data); 
        $barcode_id = $this->input->post('barcode_id');
        $barcodes = $this->input->post('barcodes');
        foreach($barcodes as $k => $value){
            $this->items_model->item_detail_update($item_id,$barcode_id,array('barcode'=>$value));
        }
        redirect('items');
    }

    // Remove Items
     public function remove_item(){
        $id = $this->input->get('id');
        $response = $this->items_model->remove_item($id);
        redirect('items');
    }
}

