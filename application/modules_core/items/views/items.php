
<div class="row">
  <div class="col-md-12">
      <div class="box">   
      <!-- Table -->
      <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Category Name</th>
              <th>Item Name</th>
              <th>Description</th>
              <th>Edit</th>
              <th>Del</th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; foreach($items as $list):?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php echo $list->category_name ?></td>
                  <td><?php echo $list->item_name ?></td>
                  <td><?php echo $list->item_description ?></td>
                  <td><a href="<?php echo base_url('items/edit_item').'?id='.$list->id;?>" class="btn btn-success btn-sm"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
                  <td><a href="<?php echo base_url('items/remove_item').'?id='.$list->id;?>" class="btn btn-warning btn-sm"><i class="fa fa-times" aria-hidden="true"></i></a></td>                  
                </tr>
            <?php endforeach;?>
          </tbody>
        </table>
    </div>  
    </div >  
  </div>
</div>
