<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Items</h3>
            </div>

            <form class="form-horizontal" method="POST" action="<?php echo base_url('items/add_items_data');?>">
            	<input type="hidden" name="id" value="<?php echo $item->id;?>" />
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Select Categories</label>
                  <div class="col-sm-6">
                  <select data-placeholder="Select Categories..." name="category_id" class="chosen-select" tabindex="2" required>
            			  <option value="<?php echo $item->id;?>"><?php echo $item->category_name;?></option>
  			            <?php foreach ($categories as $key => $value): ?>
  			            	<option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
  			            <?php endforeach;?>
        			    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Select Supliers</label>

                  <div class="col-sm-6">
                    <select data-placeholder="Select Supliers..." name="suplier_id" class="chosen-select" tabindex="2" required>
            			<option value="<?php echo $item->id;?>"><?php echo $item->category_name;?></option>
			            <?php foreach ($categories as $key => $value): ?>
			            	<option value="<?php echo $value->id;?>"><?php echo $value->name;?></option>
			            <?php endforeach;?>
        			</select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Item Name</label>

                  <div class="col-sm-6">
                    <input class="form-control" name="item_name" value="<?php echo $item->item_name;?>" placeholder="Item Name" type="text" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Item Description</label>

                  <div class="col-sm-6">
                    <textarea class="form-control" name="item_description" placeholder="Item Description"><?php echo $item->item_description;?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Item Quantity</label>

                  <div class="col-sm-6">
                    <input class="form-control" name="item_quantity" value="<?php echo $item->item_quantity;?>" placeholder="Item Quantity" type="text" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Previous Price</label>

                  <div class="col-sm-6">
                    <input class="form-control" name="previous_price" value="<?php echo $item->previous_price;?>" placeholder="Previous Price" type="text" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Serving Price</label>

                  <div class="col-sm-6">
                    <input class="form-control" name="serving_price" value="<?php echo $item->serving_price;?>" placeholder="Serving Price" type="text" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Barcode</label>

                  <div class="col-sm-6">
                    <input class="form-control" id="barcode_value" placeholder="Barcode" type="text">
                  </div>
                  <div class="col-sm-3">
                  	<button type="button" id="barcode_adder" class="btn btn-info btn-md">Enter</button>
                  </div>
                </div>

                <div class="col-md-4 pull-right" id="barcode-listing">
                	<ul class="list-group">
                		<?php foreach ($item_detail as $value): ?>
                			<li id="<?php echo $value->id;?>">
                        <span id="<?php echo $value->id;?>"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <input type="hidden" name="barcode_id[]" value="<?php echo $value->id;?>"/>
                        <input class="form-control" value="<?php echo $value->barcode;?>" name="barcodes[]" type="text" readonly>
                      </li>	
	                	<?php endforeach; ?>
					</ul>
                </div>

                
              
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info btn-md col-md-3 col-md-offset-2">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>