<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
         if(!$this->session->userdata('logged_in')){
            redirect('admin');
	    }
    }
    
	public function index(){
 
	    $data['title'] = 'dashboard';
        $data['view']  = 'dashboard';
        $this->load->view('layout',$data);
	}
}

