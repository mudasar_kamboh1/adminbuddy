<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      CodeR 
 * @copyright   (c) 2016, CMS Development
 * @since       Version 0.1
 */
class Admin_model extends My_Model {

    public function __construct() {
        parent::__construct();
    }

    //checkLogin
    public function checkLogin($data) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email',$data['email']);
        $this->db->where('password',md5($data['password']));
        $this->db->where('active_status',1);
        $query = $this->db->get();
        return $query->row();
    }
  

}
