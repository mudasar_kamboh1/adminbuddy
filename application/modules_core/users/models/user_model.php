<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Module
 * @author      CodeR 
 * @copyright   (c) 2016, CMS Development
 */
 
class User_model extends My_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_allusers(){
    	$this->db->select('*')->from('users');
    	$query = $this->db->get();
    	return $query->result();
    }
    public function get_usergroup(){
    	$this->db->select('*')->from('user_groups');
    	$query = $this->db->get();
    	return $query->result();
    }
    public function check_email($data)
    {
     
        $email = $data['email'];
        $this->db->select('email')->from('users');
        $this->db->where('email',$email);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return 1;
        }else
        {
            return 0;
        } 

    }
    public function adduser($data){
    	echo $this->db->insert('users', $data);
    }
    public function get_one_user($id){
       $this->db->select('*')->from('users')
                             ->where('id',$id);
       $query = $this->db->get();
       return $query->row();

    }
    public function delete($id){
       $this->db->where('id', $id);
       $query = $this->db->delete('users');
       echo $query;
    }

    public function updateuser($data, $id){
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

}
