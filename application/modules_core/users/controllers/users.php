<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @package     Login Controller
 * @author      Rizwan Mughal
 * @copyright   (c) 2016 CMS
 */
 
class Users extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('user_model');
        $this->load->helper('my_helper');
    }
    
    //user view
    public function index(){
        $data['title'] = 'Users';
        $data['view']  = 'user_view';
        $data['user_list'] = $this->user_model->get_allusers();
        //$data['user_group'] = $this->user_model->get_usergroup();
        $this->load->view('layout',$data);
    }
    
    //add-edit user
    public function adduser(){
        $id = $this->input->post('id');
        $data = array(
            'first_name' =>$this->input->post('first_name'),
            'last_name' =>$this->input->post('last_name'),
            'group_id' =>$this->input->post('group_id'),
            'email' =>$this->input->post('email'),
            'password' =>$this->input->post('password')
        );

        if($id){
            //Edit
            $this->user_model->updateuser($data, $id); 
            $this->session->set_flashdata('success', 'User Successfully updated');
        }else{
            $reponse = $this->user_model->check_email($data);
            if($reponse == 1){
                $this->session->set_flashdata('error', 'Email already exit!');
            }else{
                //Add
                $this->user_model->adduser($data);
                $this->session->set_flashdata('success', 'User Successfully Added'); 
            }
        }
        redirect('users');
    }

    public function get_one_user(){
     $id = $this->input->post('id');
     $data = $this->user_model->get_one_user($id);
     echo json_encode($data);
    }

    public function delete_user(){
      $id = $this->input->post('id');
      echo $this->user_model->delete($id);
    }
    
     

}
