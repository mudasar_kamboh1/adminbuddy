<div class="row">
  <div class="col-xs-12">
    <div class="box">
      
      <div class="box-header">
        <!--<h3 class="box-title">heading</h3> -->
        <!--error-->
        <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
        <?php } ?>
        <!--success-->
        <?php if ($this->session->flashdata('success')) { ?>
        <div class="alert alert-success"> <?= $this->session->flashdata('success') ?> </div>
        <?php } ?>
      
       <!-- / <button style="float:left;"  id="addUser" type="button" class="btn  btn-success btn-flat" data-toggle="modal" data-target="#userModal">Add User</button> -->
      </div>
      <!-- /.box-header -->
      
      <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
            
              <?php 
              $count = 1;
              if(!empty($user_list)): foreach ($user_list as $row): 
              ?>
            
              <tr class="odd onsccess<?php echo $row->id ?>">
                <td><?php echo $count++ ?></td>
                <td><?php echo $row->first_name.' '.$row->last_name ?></td>
                <td><?php echo $row->email ?></td>
                <td class="text-center"><?php if($row->active_status == 1){ echo '<span class="label label-success">Active</span>'; } else{ echo '<span class="label label-danger">Un Active</span>';} ?></td>
                <td class="text-center ">
                  <i class="fa fa-spinner fa-spin beforedel<?php echo $row->id ?> hide" style="font-size:24px"></i>
                  <span class="del_user<?php echo $row->id ?>">
                    <a data-id="<?php echo $row->id ?>" class="btn btn-info btn-sm edit_user pointer" title="Edit"><i class="fa fa-pencil-square"></i></a> 
                    <a data-id="<?php echo $row->id ?>" class="btn btn-danger btn-sm delete_user pointer" title="Delete"><i class="fa fa-trash"></i></a>
                  </span>
                </td>
              </tr>

              <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td colspan="5" class="text-center text-danger"><h3 >No Record Found!</h3></td>
                </tr>
              <?php endif; ?>
            </tbody>
        </table>

        
        
      <!-- /.box-body -->
    </div>

    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>


<!--userModal-->
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="userModalLabel">Add User</h3>
      </div>
      
      <div class="modal-body">
        <form id="userForm" method="POST" action="<?php echo base_url('users/adduser') ?>">
        
          <div class="row">
            <div class="form-group col-sm-6">
              <label for="recipient-name" class="control-label">First Name</label>
              <input type="text" class="form-control " id="first_name" name="first_name" required>
            </div>
            
            <div class="form-group col-sm-6">
              <label for="recipient-name" class="control-label">Last Name</label>
              <input type="text" class="form-control " id="last_name" name="last_name" required>
            </div>
          </div>
          
          <div class="form-group">
            <label>User Group</label><br>
            <label class="radio-inline">
              <input type="radio" name="group_name" id="admin" value="admin"> Admin
            </label>
            <label class="radio-inline">
              <input type="radio" name="group_name" id="user" value="user" checked="checked"> User
            </label>    
          </div>
          
          <div class="form-group">
            <label for="recipient-name" class="control-label" >Email</label>
            <input type="email" class="form-control" id="email" name="email" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Password</label>
            <input type="password" class="form-control" id="Password" name="password" required>
          </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      
      </form>
    </div>
  
  </div>
</div>
</div>