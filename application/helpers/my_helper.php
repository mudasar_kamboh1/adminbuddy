<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//total_count
if ( ! function_exists('total_count')){
	function total_count($table=""){
		if($table == ""){
			return "Table name is missing!";
		}else{
			$CI =& get_instance();
			if($CI->db->table_exists($table)){
				$query = $CI->db->query("SELECT * FROM ".$table);
				return $query->num_rows();
			}else{
				return "Table does not exist!";
			}
		}
	}
}

//get_all
if ( ! function_exists('get_all')){
	function get_all($table=""){
		if($table == ""){
			return "Table name is missing!";
		}else{
			$CI =& get_instance();
			if($CI->db->table_exists($table)){
				$CI->db->select('*');
				$CI->db->from($table);
				$query = $CI->db->get();
				return $query->result();
			}else{
				return "Table does not exist!";
			}
		}
	}
}

//get_where
if ( ! function_exists('get_single')){
	function get_where($table="",$column="",$value=""){
		if($table == ""){
			return "Table name is missing!";
		}elseif($column == ""){
			return "Column name is missing!";
		}elseif($value == ""){
			return "Value is missing!";
		}else{
			$CI =& get_instance();
			if($CI->db->table_exists($table)){
				$CI->db->select('*');
				$CI->db->from($table);
				$CI->db->where($column, $value);
				$query = $CI->db->get();
				return $query->row();
			}else{
				return "Table does not exist!";
			}
		}
	}
}