
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview <?php if($this->uri->segment(1) == 'home'):echo 'active'; endif;?>">
            <a href="<?php echo base_url('home');?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        <li class="treeview <?php if($this->uri->segment(1) == 'categories'):echo 'active';endif;?>">
          <a href="<?php echo base_url('categories');?>"><i class="fa fa-list-ul" aria-hidden="true"></i><span>Categories</span></a>
        </li>
        <li class="treeview <?php if($this->uri->segment(1) == 'items'):echo 'active';endif;?>">
          <a href="<?php echo base_url('items');?>"><i class="fa fa-dashboard"></i> <span>Items</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('items');?>"><i class="fa fa-dashboard"></i>Items</a></li>
            <li><a href="<?php echo base_url('items/add_item'); ?>"><i class="fa fa-plus-circle"></i> Add Items</a></li>            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('users') ?>"><i class="fa fa-circle-o"></i> Users</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Settings</span>
          </a>
        </li>

        
      </ul>
    </section>
    <!-- /.sidebar -->
