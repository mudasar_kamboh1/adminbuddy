<script type="text/javascript">
    $("#addUser").click(function() {
        $("#userModalLabel").html("Add User");
        $('#userForm')[0].reset();
    });
        
    $(".delete_user").click(function() {
       var id = $(this).data('id');
         $.ajax({
               type: "POST",
               url:"<?php echo base_url('users/delete_user') ?>",
               data:{id: id},
               beforeSend: function(msg){
                $(".beforedel"+id).addClass('show');
                $(".beforedel"+id).removeClass('hide');
                $(".del_user"+id).addClass('hide');
              },
               success: function(response){
                 if (response == 1) {
                 	$('.onsccess'+id).addClass('hide');
                 };
               }, 
        });
    });

    $('.edit_user').click(function() {
        $("#userModalLabel").html("Edit User");
    	var id = $(this).data('id');
        $(".EditUserId").val(id);

        $.ajax({
               type: "POST",
               url:"<?php echo base_url('users/get_one_user') ?>",
               data:{id: id},
               beforeSend: function(msg){
              
              },
               success: function(response){
                $data = JSON.parse(response);
                 if (response) {
                  $("#first_name").val($data.first_name);
                  $("#last_name").val($data.last_name);
                  $("#email").val($data.email);
                  $("#userModal").modal('show');
                 };
               }, 
        }); 
    });

    // Barcode Enter Listing Criteria
    function barcode(){
       var barcode = $('#barcode_value').val();
        if(barcode != ''){
          $('#barcode-listing').show();
          $('#barcode-listing .list-group').append('<li><input class="form-control" value="'+barcode+'" name="barcodes[]" type="text" readonly></li>');
          $('#barcode_value').val('');
          return false;  
        }
    }
    $('#barcode_adder').click(function(){
       barcode();
    });

    $('#barcode_value').keypress(function (e) {
     var key = e.which;
     if(key == 13)  // For Enter key code
      {
        barcode();
        return false; 
      }

    });   

    // End Barcode Enter Listing Criteria
    $('#barcode-listing li span').click(function(){
       var id = $(this).attr('id');
       $('#barcode-listing li#'+id).remove();
    });
</script>